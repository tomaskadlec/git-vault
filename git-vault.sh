#!/bin/bash
# This script initializes a git repository to support encryption using gpg 
# and openssl. Refer to the project website for further documentation:
# https://gitlab.com/tomaskadlec/git-vault

SCRIPT_FILE=$(python3 -c "import os; print(os.path.realpath('$0'))")
SCRIPT_DIR=$(dirname "$SCRIPT_FILE")

function usage {
    cat << --USAGE-- 1>&2

Usage: $0

Command will autodetect the state of your repo and employ changes
accordingly. 

  1. Check if .vault.gpg is present. Create it if not.
  2. Create or update .gitattributes. Entries for files that must not 
     be encrypted must be present (e.g. .gitattributes, .gitconfig
     and .vault.gpg). 
  3. Create or update .gitconfig.
  4. Include .gitconfig to local git configuration.

--USAGE--
}

function log {
    message="${1:-no message given}"
    level="${2:-info}"
    code="${3:-1}"
    if [ "$level" == 'error' ]; then
        printf "\n[%s] %s \n" "$level" "$message" 1>&2
        usage
        exit "$code"
    else
        printf "[%s] %s \n" "$level" "$message" 1>&2
    fi        
}

function gitAdd {
    git add -f "${@}"
    [ $? -ne 0 ] && log "Failed to add ${@} to index." error
    log "Files ${@} added to index."
}

function processBlock {
    local file="${1}"
    local name="${2}"
    local content="${3}"

    if [ ! -f "$file" ]; then
        createOrAppendBlock "$file" "$name" "$content"
    elif ! grep -qs "#[[:space:]]*${name}:start" "$file"; then
        createOrAppendBlock "$file" "$name" "$content"
    else
        replaceBlock "$file" "$name" "$content"
    fi
    gitAdd "$file"
}

function blockContent {
    local name="${1}"
    local content="${2}"
    cat << --BLOCK--
# ${name}:start Do not modify! Leave this at the end of file.
$(cat "$content")
# ${name}:stop
--BLOCK--
}

function createOrAppendBlock {
    local file="${1}"
    local name="${2}"
    local content="${3}"

    blockContent "$name" "$content" >> "$file"
    [ $? -ne 0 ] && log "Failed to write to $file" error
    log "Changes written to $file"
}

function replaceBlock {
    local file="${1}"
    local name="${2}"
    local content="${3}"

    ex "$file" << --EX--
/^# *${name}:start/,/^# *${name}:stop/ d
a
$(blockContent "$name" "$content")
.
w!
q
--EX--
}

function vaultPassword {
    local vaultFile='.vault.gpg'
    if [ ! -f "$vaultFile" ]; then
        openssl rand 512 | base64 | head -c 64 | gpg --encrypt --output "$vaultFile"
        [ $? -ne 0 ] && log "Failed to create $vaultFile" error
        log "File $vaultFile created"
        gitAdd "$vaultFile"
    else
        log "File $vaultFile exists"
    fi        
}

function vaultConfiguration {
    processBlock '.gitconfig' 'git-vault' "$SCRIPT_DIR/gitconfig"
    git config --local include.path '../.gitconfig'
    [ $? -ne 0 ] && log 'Failed to include git configuration' error
    log "Successfully included git configuration"
}

function vaultAttributes() {
    if [ ! -f '.gitattributes' ]; then
        echo "* filter=crypt" > ".gitattributes"
    fi
    processBlock '.gitattributes' 'git-vault' "$SCRIPT_DIR/gitattributes"
#   if grep -v "[^!]\?[[:space:]]*filter[[:space:]]*=[[:space:]]*[\"']\?crypt" .gitattributes; then
#       echo "* filter=crypt" >> ".gitattributes"
#       gitAdd ".gitattributes"
#   fi
}

function vaultNote() {
    :
}

## Help

if [ "$1" == '-h' -o "$1" == '--help' ]; then
    log "Help requested"
    usage
    exit 0
fi    

## Checks

[ ! -d .git ] && 
    log "This directory does not look like a git repository" error

git diff-index --quiet HEAD -- ||
    log "Your index contains uncommitted changes. 
         Commit or stash them prior proceeding with git-vault." error

## Initialize git-vault if necessary

vaultPassword
vaultAttributes
vaultConfiguration
