git-vault
=========

.. _git: https://git-scm.com/
.. _gpg: https://www.gnupg.org/
.. _openssl: https://www.openssl.org/
.. _python: https://www.python.org/
.. _`.gitattributes`: https://git-scm.com/docs/gitattributes
.. _`git-crypt`: https://github.com/AGWA/git-crypt   

This project is not mentioned to be used in real environment. It is just an experiment
with git_ filters and attributes and openssl_ written in BASH.  If you consider using 
encryption with git_ for real you should use git-crypt_ instead.

An encryption of one or more files on a git_ remote may be necessary. One possible
way how to achieve this it to use gpg_ and openssl_ and combine them together as a
git_ filter. `.gitattributes`_ can be used then to control which paths are encrypted
and which are not.

Installation
------------

Requirements:
  * git_ (obviously), 
  * python_, 
  * gpg_, 
  * openssl_.

Clone the repository to a location convenient to you. Create a symlink in a directory
in your ``$PATH`` to git-vault.sh

Usage
-----

Command will autodetect the state of your repo and employ changes accordingly.   

  #. Check if .vault.gpg is present. Create it if not.
  #. Create or update .gitattributes. Entries for files that must not be encrypted 
     must be present (e.g. ``.gitattributes``, ``.gitconfig`` and ``.vault.gpg``).   
  #. Create or update ``.gitconfig`` - define filters.  4. Include .gitconfig to local git configuration.

License
-------

.. _LICENSE: LICENSE

This project is distributed under MIT license. Full text can be found in the file LICENSE_
in the root directory of the project.

Resources
---------

  * https://gist.github.com/shadowhand/873637
  * https://git-scm.com/docs/gitattributes

